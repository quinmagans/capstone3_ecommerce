import React from 'react'
import { Link } from 'react-router-dom'

const Product = ({ product, col }) => {
    return (
        <div className={`col-sm-12 col-md-6 col-lg-${col} my-3`}>
            <div className="card p-3 rounded">
                <div>
                <img
                    className="card-img-top w-auto img-fluid mx-auto d-block"
                    src={product.images[0].url}
                    alt=""
                />
                </div>

                <div className="card-body d-flex flex-column">
                    <div>
                        <h6 className="card-title">
                        <Link to={`/product/${product._id}`}>{product.name}</Link>
                        </h6>
                    </div>

                    <div>
                    <p className="card-text">PHP {product.price && product.price.toLocaleString()}</p>
                    <Link to={`/product/${product._id}`} id="view_btn" className="btn btn-block">View Details</Link>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default Product
